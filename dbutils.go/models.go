/* Mero Rail API is a realtime API system by following a read map of
1. Design a REST API document. >> table describes HTTP verb, Path, Action, Resourse
2. Create models for a database. >> design crac of db project layout
3. Implement the API logic. >> Determine working models and write the code 
*/
package dbutils

// train and stations are forign keys to the schedule table
const train = `
CREATE TABLE IF NOT EXISTS train (
ID INTEGER PRIMARY KEY AUTOINCREMENT,
DRIVER_NAME VARCHAR(64) NULL,
OPERATING_STATUS BOOLEAN
)
`
const station = `
CREATE TABLE IF NOT EXISTS station (
ID INTEGER PRIMARY KEY AUTOINCREMENT,
NAME VARCHAR(64) NULL,
OPENING_TIME TIME NULL,
CLOSING_TIME TIME NULL
)
`
const schedule = `
CREATE TABLE IF NOT EXISTS schedule (
ID INTEGER PRIMARY KEY AUTOINCREMENT,
TRAIN_ID INT,
STATION_ID INT,
ARRIVAL_TIME TIME,
FOREIGN KEY (TRAIN_ID) REFERENCES train(ID),
FOREIGN KEY (STATION_ID) REFERENCES station(ID)
)`
